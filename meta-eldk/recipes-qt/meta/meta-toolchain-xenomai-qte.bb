include recipes-qt/meta/meta-toolchain-qte.bb
include recipes-core/packagegroups/nativesdk-packagegroup-sdk-host.inc

TOOLCHAIN_TARGET_TASK += "kernel-ipipe-dev xenomai-dev"
TOOLCHAIN_OUTPUTNAME = "${SDK_NAME}-toolchain-xenomai-${QTNAME}-${DISTRO_VERSION}"
