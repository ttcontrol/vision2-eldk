RDEPENDS_${PN}_append = "\
	nativesdk-bc \
	nativesdk-genext2fs \
	nativesdk-lzop \
	nativesdk-mtd-utils-jffs2 \
	nativesdk-mtd-utils-misc \
	nativesdk-mtd-utils \
	nativesdk-mtd-utils-ubifs \
	nativesdk-squashfs-tools \
	nativesdk-u-boot-mkimage \
	nativesdk-util-linux \
"
